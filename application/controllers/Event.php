<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$result = $this->event_model->fetch();
		$this->load->view('index', array('result' => $result) );
	}

	public function create()
	{
		if($this->input->post()) {
			//Insert data
			$result = $this->event_model->insert($this->input->post());
			redirect('event/index');			
		}
	}

	public function view($id)
	{
		$result = $this->event_model->fetchById($id);
		$result->occurrences = $this->event_model->calculateEventOccurrences($result);
		$result->occurrencescount = count($this->event_model->calculateEventOccurrences($result));
		$this->load->view('view', array('result' => $result) );
	}

	public function edit($id)
	{
		$result = $this->event_model->fetchById($id);
		$this->load->view('edit', array('result' => $result) );
	}

	public function delete($id)
	{
		$result = $this->event_model->delete($id);
	}

	public function update($id)
	{
		$result = $this->event_model->updateData($this->input->post(), $id);
		redirect('event/index');
	}
}
