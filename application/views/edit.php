<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Event CRUD TatvaSoft</title>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
</head>
<body>
<table id="TABLE1" class="table table-bordered">

<?php echo form_open('event/update/'.$result->id); ?>
<tr>
	<td colspan="2">
		<strong>Edit Event Page</strong>
	</td>
</tr>
<tr>
	<td>
		 Event Title:
	</td>
	<td>
		<input type="text" id="title" name="title" value="<?php echo $result->title; ?>"><br>
	</td>
</tr>
<tr>
	<td>
		 Start Date:
	</td>
	<td>
	<input type="text" id="startDate" name="startDate" value="<?php echo $result->start_date; ?>">
	</td>
</tr>
<tr>
	<td>
		 End Date:
	</td>
	<td>
	<input type="text" id="endDate" name="endDate" value="<?php echo $result->end_date; ?>">
	</td>
</tr>
<tr>
	<td>
	</td>
	<td>
	</td>
</tr>
<tr>
	<td>
		Recurrence:
	</td>
	<td>
        <select id="lstRepeatType" class="textbox-medium" name="lstRepeatType" style="font-size: x-small; width: 100px; font-family: Verdana" tabindex="10">
            <?php $selected= ''; $selected2= ''; $selected3= ''; $selected4= '';
            if($result->recurrence_field1 == 1) {
                $selected = 'selected=selected';
            }
            if($result->recurrence_field1 == 2) {
                $selected2 = 'selected=selected';
            } 
            if($result->recurrence_field1 == 3) {
                $selected3 = 'selected=selected';
            } 
            if($result->recurrence_field1 == 4) {
                $selected4 = 'selected=selected';
            }    
            ?>
			<option <?php echo $selected ?> value="1">Every</option>
			<option <?php echo $selected2 ?> value="2">Every Other</option>
			<option <?php echo $selected3 ?> value="3">Every Third</option>
			<option <?php echo $selected4 ?> value="4">Every Fourth</option>
		</select>
        <select id="lstEvery" class="textbox-medium" name="lstEvery" style="font-size: x-small; width: 66px; font-family: Verdana" tabindex="10">
            <?php $selected= ''; $selected2= ''; $selected3= ''; $selected4= '';
            if($result->recurrence_field2 == 1) {
                $selected = 'selected=selected';
            }
            if($result->recurrence_field2 == 2) {
                $selected2 = 'selected=selected';
            } 
            if($result->recurrence_field2 == 3) {
                $selected3 = 'selected=selected';
            } 
            if($result->recurrence_field2 == 4) {
                $selected4 = 'selected=selected';
            }    
            ?>
			<option <?php echo $selected; ?> value="1">Day</option>
			<option <?php echo $selected2; ?> value="2">Week</option>
			<option  <?php echo $selected3; ?>value="3">Month</option>
			<option  <?php echo $selected4; ?> value="4">Year</option>
		</select>
	</td>
</tr>

<tr>
	<td>
	</td>
	<td>
	</td>
</tr>
<tr>
	<td>
	</td>
	<td>
		<input type="submit" value="submit" class="btn btn-primary" name="submit"/>
	</td>
</tr>
</form>

<script>
  $( function() {
    $( "#startDate" ).datepicker();
  } );
  $( function() {
    $( "#endDate" ).datepicker();
  } );
  </script>
