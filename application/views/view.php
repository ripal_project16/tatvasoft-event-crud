<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"> 
</head>
<body>
<div class="container">
	<div class="row">
	<div class="col-lg-12 margin-tb">
</div>
	<table class="table table-bordered">
		<tr>
			<td colspan="2">
				<h3><strong>Event View Page</strong></h3>
			</td>
		</tr>
		<tr>
			<td>
				Event Name:
			</td>
			<td>
		<?php echo $result->title; ?>
			</td>
		</tr>
		<tr>
			<td valign="top">
				Event Occurrences:
			</td>
			<td>
				<table border=1>
				<?php foreach($result->occurrences as $occurrences) { ?>
				<tr>
					<td>
						<?php echo $occurrences; ?>
					</td>
					<td style="width: 69px">
						<?php echo date('D', strtotime($occurrences)); ?>
					</td>
				</tr>
				<?php } ?>
				</table>
			</td>
		</tr>
		<tr>
			<td>
			Total Recurrence Event: 
			</td>
			<td>
				<?php echo $result->occurrencescount; ?>
			</td>
		</tr>
	</table>
</div>
</div>
</body>
</html>