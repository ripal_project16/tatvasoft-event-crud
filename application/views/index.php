<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Event CRUD TatvaSoft</title>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css"> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
</head>
<body>
<table id="TABLE1" class="table table-bordered">

<?php echo form_open('event/create'); ?>
<tr>
	<td colspan="2">
		<h3><strong>Add Event Page</strong></h3>
	</td>
</tr>
<tr>
	<td>
		 Event Title:
	</td>
	<td>
		<input type="text" id="title" name="title" value=""><br>
	</td>
</tr>
<tr>
	<td>
		 Start Date:
	</td>
	<td>
	<input type="text" id="startDate" name="startDate">
	</td>
</tr>
<tr>
	<td>
		 End Date:
	</td>
	<td>
	<input type="text" id="endDate" name="endDate">
	</td>
</tr>
<tr>
	<td>
	</td>
	<td>
	</td>
</tr>
<tr>
	<td>
		Recurrence:
	</td>
	<td>
		<select id="lstRepeatType" class="textbox-medium" name="lstRepeatType" style="font-size: x-small; width: 100px; font-family: Verdana" tabindex="10">
			<option selected="selected" value="1">Every</option>
			<option value="2">Every Other</option>
			<option value="3">Every Third</option>
			<option value="4">Every Fourth</option>
		</select>
		<select id="lstEvery" class="textbox-medium" name="lstEvery" style="font-size: x-small; width: 66px; font-family: Verdana" tabindex="10">
			<option selected="selected" value="1">Day</option>
			<option value="2">Week</option>
			<option value="3">Month</option>
			<option value="4">Year</option>
		</select>
	</td>
</tr>

<tr>
	<td>
	</td>
	<td>
	</td>
</tr>
<tr>
	<td>
	</td>
	<td>
		<input type="submit" value="submit" name="submit" class="btn btn-primary"/>
	</td>
</tr>
</form>
<tr>
	<td colspan=2>
		<hr>
	</td>
</tr>
<tr>
	<td colspan="2">
		<strong>Event List Page</strong>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table>
		<tr>
			<td width="20">
				<strong>#</strong>
			</td>
			<td width="150">
				<strong>Title</strong>
			</td>
			<td width="250">
				<strong>Dates</strong>
			</td>
			<td width="250">
				<strong>Occurrence</strong>
			</td>
			<td width="200">
				<strong>Actions</strong>
			</td>
		</tr>
		<?php foreach($result as $row) { 
			$recurrence_field1 = ''; $recurrence_field2 = '';
			if($row->recurrence_field1 == 1) {
				$recurrence_field1 = 'Every';
			}
			if($row->recurrence_field1 == 2) {
				$recurrence_field1 = 'Every Other';
			} 
			if($row->recurrence_field1 == 3) {
				$recurrence_field1 = 'Every third';
			} 
			if($row->recurrence_field1 == 4) {
				$recurrence_field1 = 'Every Fourth';
			} 
			//
			if($row->recurrence_field2 == 1) {
				$recurrence_field2 = 'Day';
			}
			if($row->recurrence_field2 == 2) {
				$recurrence_field2 = 'Week';
			} 
			if($row->recurrence_field2 == 3) {
				$recurrence_field2 = 'Month';
			} 
			if($row->recurrence_field2 == 4) {
				$recurrence_field2 = 'Year';
			}  
			?>
			<tr>
			<td>
			<?php echo $row->id;  ?>
			</td>
			<td>
			<?php echo $row->title; ?>
			</td>
			<td>
			<?php echo $row->start_date. ' to '. $row->end_date;   ?>
			</td>
			<td>
			<?php echo $recurrence_field1 .' '. $recurrence_field2;  ?>
			</td>
			<td>
				<button class="btn btn-default"><a href="<?php echo site_url('event/view/' . $row->id); ?>">View</a></button>
				<button class="btn btn-default"><a href="<?php echo site_url('event/edit/' . $row->id); ?>">Edit</a></button>
				<button class="btn btn-default"><a href="<?php echo site_url('event/delete/' . $row->id); ?>">Delete</a></button>
			</td>
			</tr>
		<?php } ?>
		</table>
	</td>
</tr>
<tr>
	<td colspan=2>
		<hr>
	</td>
</tr>

</table>
</body>
<script>
  $( function() {
    $( "#startDate" ).datepicker();
  } );
  $( function() {
    $( "#endDate" ).datepicker();
  } );
  </script>
</html>