<?php
class Event_model extends CI_Model 
{
    /*
    * Insert Event Data and Return Inserted Id
    */
	function insert($data)
	{
        $data = array(
            'title' => $this->input->post('title'),
            'start_date' => date('Y-m-d', strtotime($this->input->post('startDate')) ),
            'end_date' => date('Y-m-d', strtotime($this->input->post('endDate'))),
            'recurrence_field1' => $this->input->post('lstRepeatType'),
            'recurrence_field2' => $this->input->post('lstEvery')
        );
        $this->db->insert('eventcrud',$data);
        return $this->db->insert_id();
    }

    /*
    * Update Event Data 
    */
    function updateData($data, $id)
	{
        $data = array(
            'title' => $this->input->post('title'),
            'start_date' => date('Y-m-d', strtotime($this->input->post('startDate')) ),
            'end_date' => date('Y-m-d', strtotime($this->input->post('endDate'))),
            'recurrence_field1' => $this->input->post('lstRepeatType'),
            'recurrence_field2' => $this->input->post('lstEvery')
        );

        $this->db->where('id', $id);
        return $this->db->update('eventcrud',$data);
    }
    
    /*
    * Fetch Event Data 
    */
    function fetch()
    {
        $query = $this->db->get('eventcrud');
        return $query->result();
    }

    /*
    * Delete Event Data By Id
    */
    function delete($id)
    {
        $this->db->delete('eventcrud', array('id' => $id) );
    }

    /*
    * Fetch Event Data By Id
    */
    function fetchById($id)
    {
        $query = $this->db->get_where('eventcrud', array('id' => $id) );
        return $query->row();
    }
    
    /*
    * calculateEventOccurrences
    */
    function calculateEventOccurrences($data)
    {
        $occurrences = array();
        return $this->calculateOccurrences($data); 
    }

    /*
    * calculateOccurrences To get event occurrence dates
    */
    function calculateOccurrences($data)
    {
        switch ($data->recurrence_field2) {
            case "1":
                //Add every day
                $incrementalDays = ($data->recurrence_field1 * $data->recurrence_field2);
                return $this->displayDates($data->start_date, $data->end_date, 'Y-m-d', ' + '. $incrementalDays .' days');
                break;
            case "2":
                $days = 7;
                $incrementalDays = ($data->recurrence_field1 * $days);
                return $this->displayDates($data->start_date, $data->end_date, 'Y-m-d', ' + '. $incrementalDays .' days');
                break;
            case "3":
                $incrementalDays = ($data->recurrence_field1 * $data->recurrence_field2);
                return $this->displayDates($data->start_date, $data->end_date, 'Y-m-d', ' + '. $data->recurrence_field1 .' months');
                // return date('Y-m-d', strtotime("+1 months", strtotime($data->start_date)));
                // echo "month";
                break;
            case "4":
                return $this->displayDates($data->start_date, $data->end_date, 'Y-m-d', ' + '. $data->recurrence_field1 .' years');
                // return date('Y-m-d', strtotime('+'.$data->recurrence_field1. 'years', strtotime($data->start_date)));
                break;
        }
    }

    /*
    * displayDates To dates from range
    */
    function displayDates($date1, $date2, $format = 'd-m-Y', $stepVal) {
        $dates = array();
        $current = strtotime($date1);
        $date2 = strtotime($date2);
        while( $current <= $date2 ) {
            $dates[] = date($format, $current);
            $current = strtotime($stepVal, $current);
        }
        return $dates;
     }
     
}